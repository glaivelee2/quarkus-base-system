/*
 Navicat Premium Data Transfer

 Source Server         : lo
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : base-system

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 24/10/2021 21:23:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_module
-- ----------------------------
DROP TABLE IF EXISTS `sys_module`;
CREATE TABLE `sys_module` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `creator` int DEFAULT NULL,
  `modifier` int DEFAULT NULL,
  `modify_time` datetime(6) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `component` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `step_type` int DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_noavfxvjvs053hbvxfixe0gw2` (`code`),
  UNIQUE KEY `UK_8h8v7p9wykuijlthspplkqg30` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sys_module
-- ----------------------------
BEGIN;
INSERT INTO `sys_module` VALUES (3, '2021-08-27 15:53:12.334000', NULL, NULL, '2021-08-27 15:53:12.334000', NULL, 'module', 'LAYOUT', '系统管理', NULL, 'system', 1, NULL, 'el-icon-menu', 1);
INSERT INTO `sys_module` VALUES (4, NULL, NULL, NULL, '2021-09-19 20:30:21.011000', NULL, 'module_tree', 'Menu', '菜单管理', 3, '/system/menu', 1, '/admin/index/menu', NULL, 1);
INSERT INTO `sys_module` VALUES (12, '2021-09-19 11:40:21.104000', NULL, NULL, '2021-09-19 11:40:21.104000', NULL, 'sysUser', 'User', '用户管理', 3, '/system/user', 1, '/admin/index/user', NULL, 1);
INSERT INTO `sys_module` VALUES (13, '2021-09-19 20:36:17.323000', NULL, NULL, '2021-09-19 20:36:17.323000', NULL, 'sysRole', 'Role', '角色管理', 3, '/system/role', 1, '/admin/index/role', NULL, 1);
INSERT INTO `sys_module` VALUES (36, NULL, NULL, NULL, NULL, NULL, 'org', 'LAYOUT', '组织管理', NULL, 'org', 1, NULL, 'el-icon-menu', 1);
INSERT INTO `sys_module` VALUES (37, NULL, NULL, NULL, NULL, NULL, 'dep', 'dep', '部门管理', 36, 'dep', 1, '/dep/list', NULL, 1);
INSERT INTO `sys_module` VALUES (38, NULL, NULL, NULL, NULL, NULL, 'post', 'post', '岗位管理', 36, 'post', 1, '/post/list', NULL, 1);
INSERT INTO `sys_module` VALUES (46, '2021-10-23 11:11:08.686000', NULL, NULL, '2021-10-23 11:11:08.686000', NULL, 'menu-add', NULL, '菜单添加', 4, NULL, 2, NULL, NULL, 1);
INSERT INTO `sys_module` VALUES (47, '2021-10-23 11:17:22.493000', NULL, NULL, '2021-10-23 11:17:22.493000', NULL, 'menu-delete', NULL, '菜单删除', 4, NULL, 2, NULL, NULL, 1);
INSERT INTO `sys_module` VALUES (48, '2021-10-23 11:17:51.582000', NULL, NULL, '2021-10-23 11:17:51.582000', NULL, 'role-add', NULL, '角色添加', 13, NULL, 2, NULL, NULL, 1);
INSERT INTO `sys_module` VALUES (49, '2021-10-23 11:18:14.823000', NULL, NULL, '2021-10-23 11:18:14.823000', NULL, 'role-delete', NULL, '角色删除', 13, NULL, 2, NULL, NULL, 1);
INSERT INTO `sys_module` VALUES (50, '2021-10-23 11:18:32.498000', NULL, NULL, '2021-10-23 11:18:32.498000', NULL, 'user-add', NULL, '用户添加', 12, NULL, 2, NULL, NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `creator` int DEFAULT NULL,
  `modifier` int DEFAULT NULL,
  `modify_time` datetime(6) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_plpigyqwsqfn7mn66npgf9ftp` (`code`),
  UNIQUE KEY `UK_bqy406dtsr7j7d6fawi1ckyn1` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, '2021-08-27 15:53:12.337000', NULL, NULL, '2021-08-27 15:53:12.337000', NULL, 'admin', '管理员');
INSERT INTO `sys_role` VALUES (5, NULL, NULL, NULL, NULL, NULL, 'admin2', '管理员1');
INSERT INTO `sys_role` VALUES (7, NULL, NULL, NULL, '2021-10-19 15:13:06.416000', '试试', 'admin4', '管理员3');
INSERT INTO `sys_role` VALUES (9, NULL, NULL, NULL, '2021-10-22 18:13:52.702000', NULL, 'admin6333', '管理员5');
INSERT INTO `sys_role` VALUES (17, NULL, NULL, NULL, NULL, NULL, 'admin15', '管理员14');
INSERT INTO `sys_role` VALUES (18, '2021-10-17 21:18:44.473000', NULL, NULL, '2021-10-17 21:57:59.863000', NULL, 'cdcdcdcd', 'e3e3e3');
INSERT INTO `sys_role` VALUES (21, '2021-10-22 18:10:39.207000', NULL, NULL, '2021-10-22 18:10:39.207000', NULL, 'ssssss', 'aaaa');
INSERT INTO `sys_role` VALUES (22, '2021-10-22 18:11:43.249000', NULL, NULL, '2021-10-22 18:11:43.249000', NULL, 'sssssswww', 'aaaaqqq');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_module
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_module`;
CREATE TABLE `sys_role_module` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `creator` int DEFAULT NULL,
  `modifier` int DEFAULT NULL,
  `modify_time` datetime(6) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `module_id` int DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sys_role_module
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_module` VALUES (454, '2021-10-22 19:36:23.380000', NULL, NULL, '2021-10-22 19:36:23.380000', NULL, 3, 9);
INSERT INTO `sys_role_module` VALUES (455, '2021-10-22 19:36:23.384000', NULL, NULL, '2021-10-22 19:36:23.384000', NULL, 4, 9);
INSERT INTO `sys_role_module` VALUES (456, '2021-10-22 19:36:23.385000', NULL, NULL, '2021-10-22 19:36:23.385000', NULL, 12, 9);
INSERT INTO `sys_role_module` VALUES (457, '2021-10-22 19:36:23.386000', NULL, NULL, '2021-10-22 19:36:23.386000', NULL, 13, 9);
INSERT INTO `sys_role_module` VALUES (458, '2021-10-22 19:37:43.399000', NULL, NULL, '2021-10-22 19:37:43.399000', NULL, 36, 7);
INSERT INTO `sys_role_module` VALUES (459, '2021-10-22 19:37:43.401000', NULL, NULL, '2021-10-22 19:37:43.401000', NULL, 37, 7);
INSERT INTO `sys_role_module` VALUES (460, '2021-10-22 19:37:43.402000', NULL, NULL, '2021-10-22 19:37:43.402000', NULL, 38, 7);
INSERT INTO `sys_role_module` VALUES (461, '2021-10-22 21:54:56.302000', NULL, NULL, '2021-10-22 21:54:56.302000', NULL, 36, 22);
INSERT INTO `sys_role_module` VALUES (462, '2021-10-22 21:54:56.307000', NULL, NULL, '2021-10-22 21:54:56.307000', NULL, 37, 22);
INSERT INTO `sys_role_module` VALUES (463, '2021-10-22 21:54:56.308000', NULL, NULL, '2021-10-22 21:54:56.308000', NULL, 38, 22);
INSERT INTO `sys_role_module` VALUES (474, '2021-10-23 11:19:18.722000', NULL, NULL, '2021-10-23 11:19:18.722000', NULL, 3, 1);
INSERT INTO `sys_role_module` VALUES (475, '2021-10-23 11:19:18.725000', NULL, NULL, '2021-10-23 11:19:18.725000', NULL, 4, 1);
INSERT INTO `sys_role_module` VALUES (476, '2021-10-23 11:19:18.726000', NULL, NULL, '2021-10-23 11:19:18.726000', NULL, 46, 1);
INSERT INTO `sys_role_module` VALUES (477, '2021-10-23 11:19:18.726000', NULL, NULL, '2021-10-23 11:19:18.726000', NULL, 47, 1);
INSERT INTO `sys_role_module` VALUES (478, '2021-10-23 11:19:18.727000', NULL, NULL, '2021-10-23 11:19:18.727000', NULL, 12, 1);
INSERT INTO `sys_role_module` VALUES (479, '2021-10-23 11:19:18.728000', NULL, NULL, '2021-10-23 11:19:18.728000', NULL, 50, 1);
INSERT INTO `sys_role_module` VALUES (480, '2021-10-23 11:19:18.729000', NULL, NULL, '2021-10-23 11:19:18.729000', NULL, 13, 1);
INSERT INTO `sys_role_module` VALUES (481, '2021-10-23 11:19:18.729000', NULL, NULL, '2021-10-23 11:19:18.729000', NULL, 48, 1);
INSERT INTO `sys_role_module` VALUES (482, '2021-10-23 11:19:18.730000', NULL, NULL, '2021-10-23 11:19:18.730000', NULL, 49, 1);
INSERT INTO `sys_role_module` VALUES (483, '2021-10-23 11:19:18.731000', NULL, NULL, '2021-10-23 11:19:18.731000', NULL, 36, 1);
INSERT INTO `sys_role_module` VALUES (484, '2021-10-23 11:19:18.732000', NULL, NULL, '2021-10-23 11:19:18.732000', NULL, 37, 1);
INSERT INTO `sys_role_module` VALUES (485, '2021-10-23 11:19:18.733000', NULL, NULL, '2021-10-23 11:19:18.733000', NULL, 38, 1);
INSERT INTO `sys_role_module` VALUES (486, '2021-10-23 11:19:52.489000', NULL, NULL, '2021-10-23 11:19:52.489000', NULL, 12, 5);
INSERT INTO `sys_role_module` VALUES (487, '2021-10-23 11:19:52.491000', NULL, NULL, '2021-10-23 11:19:52.491000', NULL, 50, 5);
INSERT INTO `sys_role_module` VALUES (488, '2021-10-23 11:19:52.492000', NULL, NULL, '2021-10-23 11:19:52.492000', NULL, 36, 5);
INSERT INTO `sys_role_module` VALUES (489, '2021-10-23 11:19:52.492000', NULL, NULL, '2021-10-23 11:19:52.492000', NULL, 37, 5);
INSERT INTO `sys_role_module` VALUES (490, '2021-10-23 11:19:52.493000', NULL, NULL, '2021-10-23 11:19:52.493000', NULL, 38, 5);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `creator` int DEFAULT NULL,
  `modifier` int DEFAULT NULL,
  `modify_time` datetime(6) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `user_pwd` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ahtq5ew3v0kt1n7hf1sgp7p8l` (`email`),
  UNIQUE KEY `UK_cb0fsvip6qow952a07et2k9xv` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` VALUES (10, '2021-08-27 15:53:12.352000', NULL, NULL, '2021-08-27 15:53:12.352000', NULL, '22222@qq.com', 'weiwei', 'DimebHZ8B8RLhcf4bCvhEw==', 1);
INSERT INTO `sys_user` VALUES (11, '2021-09-19 13:41:45.497000', NULL, NULL, '2021-09-19 13:41:45.497000', NULL, '634623907@qq.com', 'weir11', 'ANPsg6YudoEJjKE0Ln555MacOSZgpCuVcdw2vmbSePw=', 1);
INSERT INTO `sys_user` VALUES (13, '2021-09-19 13:46:49.613000', NULL, NULL, '2021-09-19 13:46:49.613000', NULL, '6346333907@qq.com', 'eeeewww', 'w3IMP/fKEGdzioA7jEUfyozn3Q0j67cVn+jSLJJm8U2jT/AB1JK/6LHLt3e3/ZNd', 1);
INSERT INTO `sys_user` VALUES (14, '2021-09-19 13:48:07.636000', NULL, NULL, '2021-09-19 13:48:07.636000', NULL, '63462223907@qq.com', 'qqww33', 'ANPsg6YudoEJjKE0Ln555MacOSZgpCuVcdw2vmbSePw=', 1);
INSERT INTO `sys_user` VALUES (15, '2021-09-19 21:37:27.718000', NULL, NULL, '2021-09-19 21:37:27.718000', NULL, '63462390qq7@qq.com', 'ss222', '4iifEqx3+dvjRZCABGaZug==', 1);
INSERT INTO `sys_user` VALUES (16, '2021-09-19 21:43:36.660000', NULL, NULL, '2021-09-19 21:43:36.660000', NULL, '634111623907@qq.com', 'eeee22', 'yja8D4h056Nyhq5CLQm8ew==', 1);
INSERT INTO `sys_user` VALUES (17, '2021-09-19 21:44:40.289000', NULL, NULL, '2021-09-19 21:44:40.289000', NULL, '634623dd907@qq.com', 'dds', 'mVO5nwZbjr6Edb/RA7sNvQ==', 1);
INSERT INTO `sys_user` VALUES (18, '2021-09-20 09:39:30.438000', NULL, NULL, '2021-09-20 09:39:30.438000', NULL, '634222623907@qq.com', '11q1q', 'R0OECfwQtKqQ1EeR6hKmiw==', 1);
INSERT INTO `sys_user` VALUES (19, '2021-09-20 09:42:25.742000', NULL, NULL, '2021-09-20 09:42:25.742000', NULL, 'aqqqqqwsx', 'xxsx', '4W6cWyrY+RjBBVWcmFFB7Q==', 1);
INSERT INTO `sys_user` VALUES (20, '2021-09-21 08:37:44.183000', NULL, NULL, '2021-09-21 08:37:44.183000', NULL, '6346xxx23907@qq.com', 'dcdcdc', 'GKCjrAmh4KUTKsWFURPN7A==', 1);
INSERT INTO `sys_user` VALUES (21, '2021-09-21 08:50:22.786000', NULL, NULL, '2021-10-22 11:45:00.334000', NULL, 'cddc222222@qq.com', 'eddd22', '0SrOOUIBUTByO1NJr+UVzg==', 0);
INSERT INTO `sys_user` VALUES (22, '2021-09-21 09:00:44.280000', NULL, NULL, '2021-09-21 09:00:44.280000', NULL, 'qqqqq@qq.com', 'qqqq', 'uPB+2+mCpUz96f/rltiuhA==', 1);
INSERT INTO `sys_user` VALUES (23, '2021-09-21 09:02:42.791000', NULL, NULL, '2021-09-21 09:02:42.791000', NULL, 'swwwwwww', 'wwwwww', '8WVNP8fl10fLAtum+P8Fow==', 0);
INSERT INTO `sys_user` VALUES (37, '2021-10-23 10:31:08.774000', NULL, NULL, '2021-10-23 10:31:08.774000', NULL, '224455556@qq.com', 'ssss1111', 'DimebHZ8B8RLhcf4bCvhEw==', 1);
INSERT INTO `sys_user` VALUES (38, '2021-10-23 10:32:37.856000', NULL, NULL, '2021-10-23 10:32:37.856000', NULL, '1121qqqq@qq.com', 'qqq111', 'DimebHZ8B8RLhcf4bCvhEw==', 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `creator` int DEFAULT NULL,
  `modifier` int DEFAULT NULL,
  `modify_time` datetime(6) DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` VALUES (1, '2021-08-27 15:53:12.356000', NULL, NULL, '2021-08-27 15:53:12.356000', NULL, 1, 10);
INSERT INTO `sys_user_role` VALUES (3, '2021-09-20 18:22:26.887000', NULL, NULL, '2021-09-20 18:22:26.887000', NULL, 1, 11);
INSERT INTO `sys_user_role` VALUES (10, '2021-10-23 11:00:04.238000', NULL, NULL, '2021-10-23 11:00:04.238000', NULL, 5, 38);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
