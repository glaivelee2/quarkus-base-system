package com.weir.quarkus.base.system.vo;

public class ElementPageVo {

	public Integer page;
	public Integer pageSize;
	public Long total;
}
