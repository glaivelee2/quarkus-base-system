package com.weir.quarkus.base.system.entity;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

/**
 * 
 * @ClassName: SysUser
 * @Description: 后台用户实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_user")
public class SysUser extends BaseEntity {
	private static final long serialVersionUID = -181390024194022960L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	// 唯一性
	@Column(unique = true)
	@NotBlank
    public String email;
	
	@Column(unique = true, name = "user_name")
	@NotBlank
    public String userName;
	
	// 密码加密保存和解密展示
//	@Convert(converter = EncryptConverter.class)
	@Column(name = "user_pwd")
    public String userPwd;
	
	public boolean enable = true;
	
	@Transient
	public Set<String> moduleCodes;
	
	@Transient
	public String token;
	@Transient
	public List<Integer> roleList;

	public Integer getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

}