package com.weir.quarkus.base.system.vo;

import java.util.ArrayList;
import java.util.List;

public class VueMenuVo {

	public Integer id;
	public String path;
	public String name;
	public String component;
	public String redirect;
	public VueMenuMetaVo meta;
	
	public List<VueMenuVo> children = new ArrayList<>();
}
