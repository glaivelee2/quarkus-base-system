package com.weir.quarkus.base.system.handler;

import com.weir.quarkus.base.system.entity.SysUser;
import com.weir.quarkus.base.system.utils.TokenUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @ClassName: AuthorizationFilter
 * @Description: url权限拦截
 * @author weir
 * @date 2021年8月28日
 *
 */
@Provider
public class AuthorizationFilter implements ContainerRequestFilter {

	@ConfigProperty(name = "authorization.permit-patterns")
	String permitPatterns;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) {
    	String[] splits = StringUtils.split(permitPatterns, ",");
    	List<String> list = Arrays.asList(splits);
    	String path = containerRequestContext.getUriInfo().getPath();
    	if (list.stream().anyMatch(path::contains)) {
			return;
		}
        Map<String, List<String>> headers = containerRequestContext.getHeaders();
        List<String> token = headers.get("Authorization");
        if (token == null || token.isEmpty()) {
//        	throw new RuntimeException("No Authorization");
		}

//        try {
//			SysUser user = TokenUtils.getUser(token.get(0));
//			if (user == null) {
//				throw new RuntimeException("非法用户");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
    }
//    @Override
//    public void filter(ContainerRequestContext containerRequestContext) {
//    	String[] splits = StringUtils.split(permitPatterns, ",");
//    	List<String> list = Arrays.asList(splits);
//    	String path = containerRequestContext.getUriInfo().getPath();
//    	if (list.stream().anyMatch(path::contains)) {
//    		return;
//    	}
//    	Map<String, Cookie> cookies = containerRequestContext.getCookies();
//    	Cookie cookieToken = cookies.get("weir-token");
//    	if (cookieToken == null) {
//    		try {
//    			containerRequestContext.setRequestUri(new URI("admin/index"));
//    		} catch (URISyntaxException e) {
//    			e.printStackTrace();
//    		}
//    	}
//    	
//    }


}