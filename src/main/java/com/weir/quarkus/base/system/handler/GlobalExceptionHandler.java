package com.weir.quarkus.base.system.handler;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import io.vertx.core.json.JsonObject;

@Provider
public class GlobalExceptionHandler implements ExceptionMapper<Throwable> {
    
    @Override
    public Response toResponse(Throwable e) {
        return Response.status(Response.Status.BAD_REQUEST).entity(new JsonObject().put("message", e.getMessage())).build();
    }
    
}
