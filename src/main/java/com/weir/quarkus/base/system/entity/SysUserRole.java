package com.weir.quarkus.base.system.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @ClassName: SysUserRole
 * @Description: 后台用户角色中间实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_user_role")
public class SysUserRole extends BaseEntity {
	private static final long serialVersionUID = -2810662355177441592L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(name = "user_id")
	public Integer userId;
	@Column(name = "role_id")
	public Integer roleId;
	public Integer getRoleId() {
		return roleId;
	}
	public SysUserRole(Integer userId, Integer roleId) {
		super();
		this.userId = userId;
		this.roleId = roleId;
	}
	public SysUserRole() {
		super();
	}
	

}