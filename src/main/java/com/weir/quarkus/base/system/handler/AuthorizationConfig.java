//package com.weir.quarkus.base.system.handler;
//
//import io.smallrye.config.ConfigMapping;
//
//@ConfigMapping(prefix = "authorization", namingStrategy = ConfigMapping.NamingStrategy.KEBAB_CASE)
//public interface AuthorizationConfig {
//
//    /**
//     * 无需鉴权的路径Pattern
//     * @return
//     */
//    String permitPatterns();
//
//}