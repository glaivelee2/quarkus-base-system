package com.weir.quarkus.base.system.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @ClassName: SysRoleModule
 * @Description: 后台角色菜单中间实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_role_module")
public class SysRoleModule extends BaseEntity {
	private static final long serialVersionUID = -1314835402340023418L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(name = "role_id")
	public Integer roleId;
	@Column(name = "module_id")
	public Integer moduleId;
	public SysRoleModule(Integer roleId, Integer moduleId) {
		super();
		this.roleId = roleId;
		this.moduleId = moduleId;
	}
	public SysRoleModule() {
		super();
	}
	public Integer getModuleId() {
		return moduleId;
	}

}