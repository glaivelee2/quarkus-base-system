package com.weir.quarkus.base.system.vo;

import java.util.List;

public class VuePageListVo<T> {

	public List<T> list;
	public Integer page = 1;
	public Integer pageSize = 10;
	public Long pageCount = 0L;
	
	public ElementPageVo pageEl;
}
