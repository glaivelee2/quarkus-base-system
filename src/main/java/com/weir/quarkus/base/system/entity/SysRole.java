package com.weir.quarkus.base.system.entity;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

/**
 * 
 * @ClassName: SysRole
 * @Description: 后台角色实体
 * @author weir
 * @date 2021年8月25日
 *
 */
@Entity
@Table(name = "sys_role")
public class SysRole extends BaseEntity {
	private static final long serialVersionUID = -7926845745350235387L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer id;
	@Column(unique = true)
	@NotBlank
    public String name;
	
	@Column(unique = true)
	@NotBlank
    public String code;

	@Transient
	public List<Integer> menuKeys;

	public Integer getId() {
		return id;
	}
}